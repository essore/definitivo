var foto = localStorage.nomefoto;
var def_lat=43.58;
var def_long=13.51;
var timer=0;
var longit = localStorage.Longitude;
var latit = localStorage.Latitude;
cartella = "Geofolder";
if(localStorage.Timer >=0 ){
    timer=localStorage.Timer;
}

document.addEventListener("deviceready",onDeviceReady,false);

function onDeviceReady() {
    navigator.geolocation.getCurrentPosition(recupera, posError,{timeout:20000});
    getFile();
}

//location//
function recupera(position) {
    localStorage.Latitude=position.coords.latitude;
    localStorage.Longitude=position.coords.longitude;
    localStorage.Timer=Date.now()/1000;
    longit = localStorage.Longitude;
    latit = localStorage.Latitude;
    timer=localStorage.Timer;
    document.forms['up'].latit.value=latit;
    document.forms['up'].longit.value=longit;
    document.forms['up'].foto.value=foto;
}

function posError() {
    var now = Date.now()/1000;         //timestamp in secondi
    if(now - timer > 900){         //900 secondi sono 15 minuti
        //se l'ultima posizione è più vecchia di 15 minuti uso la posizione default
        latit=def_lat;
        longit=def_long;
        document.forms['up'].latit.value=latit;
        document.forms['up'].longit.value=longit;
    }else {
        //altrimenti uso l'ultima posizione salvata sul local storage
        alert("attenzione, rilevamento posizione disabilitato");
        longit = localStorage.Longitude;
        latit = localStorage.Latitude;
        document.forms['up'].latit.value=latit;
        document.forms['up'].longit.value=longit;
    }
    document.forms['up'].foto.value=foto;

}


function uploadimage() {
    var Image = document.getElementById('Image').getAttribute("src");
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=foto; //nome della foto che verra salvato online
    options.mimeType="image/jpeg";

    var params = new Object();
    params.nome = "test"; //??

    options.params = params;
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(Image, "http://cuccandri.altervista.org/uploadphoto.php", win, fail, options);
    return false;
}

function win(r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    alert("upload foto completato!");
}

function fail(error) {
    //alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    alert("ops! il server non risponde!");

}

function getFile(){
    path= "/"+cartella+"/"+foto;
    //alert("get file: stai per caricare: " + path);
    immettifoto(path);
    /*
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

        fs.root.getFile(path , { create: false, exclusive: false }, function (fileEntry) {

            console.log("fileEntry is file?" + fileEntry.isFile.toString());
            // fileEntry.name == 'someFile.txt'
            // fileEntry.fullPath == '/someFile.txt'

            immettifoto(fileEntry, null);

        }, fallito("fs.root.getFile"));

    }, fallito("load fs"));
    */
}

function fallito(error) {
    alert("ha fallito: "+error);
}

function immettifoto(fileEntry) {
    //metto il nome dentro alla form

    var path = "../../storage/emulated/0/"+ fileEntry;

    var Image = document.getElementById('Image');
    Image.src = path;
   // alert("immetti foto :"+ path);
}

$("#upload").click(function(){
    var user= localStorage.username;
    var criticita = 0;
    if(document.getElementById('cr-bassa').checked) {
        criticita = 1;
    }else if(document.getElementById('cr-media').checked) {
        criticita = 2;
        //alert("cr 2");
    }else if(document.getElementById('cr-alta').checked) {
        criticita = 3;
        //alert("cr 3");
    }

    var Urge= document.getElementById('Urgenza');
    var note = $("#Note").val();

    var urgenza='0';
    if (Urge.checked){
        urgenza='1';
    }

    salvaimage(user, criticita, urgenza,latit,longit,note); //salva i dati in local storage
    uploadimage();
   // alert("user "+user + " latit e longit "+latit+longit+" crit "+criticita+" urg "+urgenza+" note " + note+ " foto "+foto);
    $.ajax({
        type: "POST",
        url: "http://cuccandri.altervista.org/upload.php",
        data: {user: user, longit: longit, latit: latit, criticita: criticita, urgenza: urgenza, note: note, foto: foto},
        success: function(responce){
            if(responce=='success')
            {
                alert('upload completato, controlla la mappa!');
                window.location.replace("mappa.html");

            }
            else
            {
                alert("errore, il server non risponde, riprova più tardi.");
                window.location.replace("galleria.html");
            }
        }
    });
    return false;
});


function salvaimage(user, criticita, urgenza,latit,longit,note) {

    var oldItems = JSON.parse(localStorage.getItem('segnalazioni')) || [];

    var newItem = { "user":user, "latit": latit, "longit":longit, "criticita": criticita, "urgenza": urgenza, "note" : note, "foto" : foto };

    oldItems.push(newItem);

    localStorage.setItem('segnalazioni', JSON.stringify(oldItems));
    //alert("dati salvati in locale");
 }
