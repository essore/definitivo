var pictureSource;   // picture source
var destinationType; // sets the format of returned value

document.addEventListener("deviceready",onDeviceReady,false);

function getData() {
    now = new Date();
    year = "" + now.getFullYear();
    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second;
}



function onDeviceReady() {
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
}

function capturePhoto() {
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI
    });
}

function getPhoto(source) {
    //alert("1b: prende da galleria");
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source    });
}

function onPhotoDataSuccess(imageURI) {
    window.resolveLocalFileSystemURI(imageURI, resolveOnSuccess, resOnError);

    //movePic(imageURI);
}

function onFail(message) {
    alert('Failed because: ' + message);
}
/*
function movePic(file){ //non la uso più
    alert("3: move pic");
    window.resolveLocalFileSystemURI(file, resolveOnSuccess, resOnError);
}
*/
//Callback function when the file system uri has been resolved
function resolveOnSuccess(entry){
    //alert("4:resolve on success: "+ entry.fullPath);
    var n = getData();
    var newFileName = n + ".jpg";
    var myFolderApp = "Geofolder";
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSys) {
            //creo la cartella geofolder e salvo le foto scattate
            fileSys.root.getDirectory(myFolderApp,
                {create:true, exclusive: false},
                function(myFolderApp) {
                    localStorage.nomefoto = newFileName;
                    entry.moveTo(myFolderApp, newFileName,  successMove, resOnError);
                },
                resOnError);
        },
        resOnError);
    setTimeout(successMove, 2000);

    return newFileName;
}

//Callback function when the file has been moved successfully - inserting the complete path
function successMove(entry) {
    //alert("5: success move, redirecting...");
    //sessionStorage.setItem('imagepath', entry.fullPath);
    window.location.replace("form.html");
}

function resOnError() {
    //alert("resonerror");
    //window.location.replace("formica.html");

}

/*
/////posizione/////

function recupera(position) {
    localStorage.Latitude=position.coords.latitude;
    localStorage.Longitude=position.coords.longitude;
    localStorage.Timer=Date.now()/1000;
    longit = localStorage.Longitude;
    Latit = localStorage.Latitude;
    timer=localStorage.Timer;
    document.forms['up'].latit.value=latit;
    document.forms['up'].longit.value=longit;
}

function posError() {
    var now = Date.now()/1000;         //timestamp in secondi
    if(now - timer > 900){         //900 secondi sono 15 minuti
        //se l'ultima posizione è più vecchia di 15 minuti uso la posizione default
        Latit=def_lat;
        Longit=def_long;
        document.forms['up'].Latitudine.value=Latit;
        document.forms['up'].Longitudine.value=longit;
    }else {
        //altrimenti uso l'ultima posizione salvata sul local storage
        longit = localStorage.Longitude;
        latit = localStorage.Latitude;
        document.forms['up'].Latitudine.value=Latit;
        document.forms['up'].Longitudine.value=longit;
    }
}

////upload///

$("#upload").click(function(){
    var user= localStorage.username;
    var longit = $("#longit").val();
    var latit = $("#latit").val();
    var criticita = $("#criticita").val();
    var Urge= document.getElementById('urgenza');
    var note = $("#note").val();
    var foto = $("#foto").val();

    if (Urge.checked){
        var urgenza='1';
    }else{
        var urgenza='0';
    }
    uploadimage(foto);
    $.ajax({
        type: "POST",
       // url: "http://cuccandri.altervista.org/upload.php",
        url: "http://cuccandri.altervista.org/test.php",
        data: {user: user, longit: longit, latit: latit, criticita: criticita, urgenza: urgenza, note: note, foto: foto},
        success: function(responce){
            if(responce=='success')
            {
                alert('ok');
            }
            else
            {
                alert("errore");

            }
        }
    });
    return false;});

function uploadimage(foto) {

    var Image = document.getElementById('Image').getAttribute("src");

    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=foto;
//    options.fileName=Image.substr(Image.lastIndexOf('/')+1);
    options.mimeType="image/jpeg";

    var params = new Object();
    params.nome = "test";

    options.params = params;
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(Image, "http://cuccandri.altervista.org/uploadphoto.php", win, fail, options);
}

function win() {
    alert("ok bro");
}
function fail() {
    alert("not bro");

}
    */